
local noneFraction = addFraction(0, "Brak");

PlayerClass(0, "Nieznajomy", function(pid) {
    setPlayerHealth(pid, 100);
    setPlayerMaxHealth(pid, 100);

    setPlayerDexterity(pid, 10);
    setPlayerStrength(pid, 10);

    setPlayerSkillWeapon(pid, 0, 10);
    setPlayerSkillWeapon(pid, 1, 10);
    setPlayerSkillWeapon(pid, 2, 10);
    setPlayerSkillWeapon(pid, 3, 10);

    giveItem(pid, "ITAR_PRISONER", 1);
    equipItem(pid, Items.id("ITAR_PRISONER"));
}, noneFraction)

local townFraction = addFraction(1, "Miasto");

PlayerClass(0, "Obywatel", function(pid) {
    setPlayerHealth(pid, 130);
    setPlayerMaxHealth(pid, 130);

    setPlayerDexterity(pid, 10);
    setPlayerStrength(pid, 10);

    setPlayerSkillWeapon(pid, 0, 10);
    setPlayerSkillWeapon(pid, 1, 10);
    setPlayerSkillWeapon(pid, 2, 10);
    setPlayerSkillWeapon(pid, 3, 10);

    giveItem(pid, "ITAR_VLK_L", 1);
    equipItem(pid, Items.id("ITAR_VLK_L"));
}, townFraction)

PlayerClass(1, "Stra�nik", function(pid) {
    setPlayerHealth(pid, 250);
    setPlayerMaxHealth(pid, 250);

    setPlayerDexterity(pid, 40);
    setPlayerStrength(pid, 40);

    setPlayerSkillWeapon(pid, 0, 30);
    setPlayerSkillWeapon(pid, 1, 30);
    setPlayerSkillWeapon(pid, 2, 30);
    setPlayerSkillWeapon(pid, 3, 30);

    giveItem(pid, "ITMW_1H_SLD_SWORD", 1)
    giveItem(pid, "ITAR_VLK_L", 1);
    equipItem(pid, Items.id("ITAR_VLK_L"));
    equipItem(pid, Items.id("ITMW_1H_SLD_SWORD"));
}, townFraction)
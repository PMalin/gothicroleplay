zType <- {
    Wilk = {
        name = "Wilk",
        instance = "WOLF",
        str = 30,
        dex = 30,
        hp = 100,
        hpMax = 100,
        exp = 70,
        spawnTime = (10*2.5), // 120 sekund
    },
    Krwio = {
        name = "Krwiopijca",
        instance = "BLOODFLY",
        str = 15,
        dex = 15,
        hp = 50,
        hpMax = 50,
        exp = 50,
        spawnTime = (60*2.5)
    },
    Polna = {
        name = "Polna bestia",
        instance = "GIANT_BUG",
        str = 25,
        dex = 25,
        hp = 50,
        hpMax = 50,
        exp = 45,
        spawnTime = (240*2.5)
    },
    Szczur = {
        name = "Szczur",
        instance = "GIANT_RAT",
        str = 20,
        dex = 20,
        hp = 50,
        hpMax = 50,
        exp = 60,
        spawnTime = (120*2.5)
    },
    MlodeScierwo  = {
        name = "Mlody Scierwojad",
        instance = "SCAVENGER",
        str = 10,
        dex = 10,
        hp = 20,
        hpMax = 20,
        exp = 35,
        spawnTime = (60*2.5)
    },
    MlodyWilk  = {
        name = "Mlody Wilk",
        instance = "YWOLF",
        str = 15,
        dex = 15,
        hp = 40,
        hpMax = 40,
        exp = 45,
        spawnTime = (60*2.5)
    },
    Goblin = {
        name = "Goblin",
        instance = "YGOBBO_GREEN",
        str = 25,
        dex = 25,
        hp = 60,
        hpMax = 60,
        exp = 50,
        spawnTime = (120*2.5)        
    },
    Goblin = {
        name = "Goblin",
        instance = "YGOBBO_GREEN",
        str = 25,
        dex = 25,
        hp = 60,
        hpMax = 60,
        exp = 50,
        spawnTime = (120*2.5)        
    },
    CzarnyGoblin = {
        name = "Czarny Goblin",
        instance = "YGOBBO_BLACK",
        str = 45,
        dex = 45,
        hp = 60,
        hpMax = 60,
        exp = 70,
        spawnTime = (150*2.5)            
    },
    Zebacz = {
        name = "Zebacz",
        instance = "SNAPPER",
        str = 85,
        dex = 85,
        hp = 220,
        hpMax = 220,
        exp = 200,
        spawnTime = (300*2.5)            
    },
    Cienio = {
        name = "Cieniostwor",
        instance = "SHADOWBEAST",
        str = 150,
        dex = 150,
        hp = 400,
        hpMax = 400,
        exp = 400,
        spawnTime = (30*60*2.5)            
    }
    Bandit = {
        name = "Bandyta",
        instance = "PC_HERO",
        str = 150,
        dex = 150,
        hp = 400,
        hpMax = 400,
        exp = 400,
        melee = "ITMW_1H_SLD_AXE",
        ranged = -1,
        armor = "ITAR_SLD_L",
        weapon = [60, 60, 0, 0],
        spawnTime = (30*60*2.5)        
    }   
}
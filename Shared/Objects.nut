

local obj = addObject("CHECK_WORK");

obj.addCall(OBJECT.DISTANCE, {
    position = { x = 0, y = 0 , z = 0, distance = 1000},
    onEnterMessage = "You entered working field press O.", // Translation resource.
    onExitMessage = "You exit working field press O.",
})

obj.addCall(OBJECT.KEY, {
    key = KEY_O,
})

local obj = addObject("TREASURE_GROUND");

obj.addCall(OBJECT.DISTANCE, {
    position = { x = 5555, y = 0 , z = 0, distance = 200},
})

obj.addCall(OBJECT.ITEM, {
    item = { instance = "ITMW_2H_AXE_L_01", amount = 1 },
})

obj.addCall(OBJECT.KEY, {
    key = KEY_O,
})

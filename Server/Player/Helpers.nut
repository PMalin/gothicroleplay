_setPlayerStrength <- setPlayerStrength;
_setPlayerDexterity <- setPlayerDexterity;
_setPlayerHealth <- setPlayerHealth;
_setPlayerMaxHealth <- setPlayerMaxHealth;
_setPlayerMaxMana <- setPlayerMaxMana;
_setPlayerMagicLevel <- setPlayerMagicLevel;
_setPlayerSkillWeapon <- setPlayerSkillWeapon;
_setPlayerVisual <- setPlayerVisual;

function setPlayerStrength(pid, val)
{
	Player[pid].str = val;
	_setPlayerStrength(pid, val);
}

function getPlayerStrength(pid)
{
    return Player[pid].str;
}

function setPlayerDexterity(pid, val)
{
	Player[pid].dex = val;
	_setPlayerDexterity(pid, val);
}

function getPlayerDexterity(pid)
{
    return Player[pid].dex;
}

function setPlayerMaxMana(pid, val)
{
	Player[pid].manaMax = val;
	_setPlayerMaxMana(pid, val);
}

function getPlayerMaxMana(pid)
{
    return Player[pid].manaMax;
}

function completeHealth(pid)
{
	setPlayerHealth(pid, getPlayerMaxHealth(pid));
}

function setPlayerHealth(pid, val)
{
	if(val < 0)
		val = 0;

	_setPlayerHealth(pid, val)
}

function setPlayerMaxHealth(pid, val)
{
	Player[pid].hpMax = val;
	_setPlayerMaxHealth(pid, val)
}

function getPlayerMaxHealth(pid)
{
    return Player[pid].hpMax;
}

function setPlayerMagicLevel(pid, val)
{
    Player[pid].magicLvl = val;
	_setPlayerMagicLevel(pid, val);
}

function getPlayerMagicLevel(pid)
{
    return Player[pid].magicLvl;
}

function setPlayerSkillWeapon(pid, id, val)
{
	Player[pid].weapon[id] = val;
	_setPlayerSkillWeapon(pid, id, val);
}

function getPlayerSkillWeapon(pid, id)
{
    return Player[pid].weapon[id];
}

function setPlayerWalkingStyle(pid, id)
{
	removePlayerOverlay(pid, Mds.id("HUMANS_MAGE.MDS"));
	removePlayerOverlay(pid, Mds.id("HUMANS_BABE.MDS"));
	removePlayerOverlay(pid, Mds.id("HUMANS_MILITIA.MDS"));
	removePlayerOverlay(pid, Mds.id("HUMANS_RELAXED.MDS"));
	removePlayerOverlay(pid, Mds.id("HUMANS_ARROGANCE.MDS"));
	removePlayerOverlay(pid, Mds.id("HUMANS_TIRED.MDS"));

	switch(id)
	{
		case 1:
			applyPlayerOverlay(pid, Mds.id("HUMANS_TIRED.MDS"));
		break;
		case 2:
			applyPlayerOverlay(pid, Mds.id("HUMANS_BABE.MDS"));
		break;
		case 4:
			applyPlayerOverlay(pid, Mds.id("HUMANS_MILITIA.MDS"));
		break;
		case 3:
			applyPlayerOverlay(pid, Mds.id("HUMANS_RELAXED.MDS"));
		break;
		case 5:
			applyPlayerOverlay(pid, Mds.id("HUMANS_MAGE.MDS"));
		break;
	}
	Player[pid].walk = id;
}

function getPlayerWalkingStyle(pid)
{
    return Player[pid].walk;
}

function setPlayerVisual(pid, body, skin, head, face)
{
	Player[pid].body = body;
	Player[pid].skin = skin;
	Player[pid].head = head;
	Player[pid].face = face;
	_setPlayerVisual(pid, body, skin, head, face)
}

function getDistancePlayerToPlayer(playerOne, playerTwo)
{
	local posOne = getPlayerPosition(playerOne);
	local posTwo = getPlayerPosition(playerTwo);

	return getDistance3d(posOne.x, posOne.y, posOne.z, posTwo.x, posTwo.y, posTwo.z);
}

function setPlayerDescription(pid, value)
{
	Player[pid].description = value;
	
	local packet = Packet()
	packet.writeChar(PacketId.Player);
	packet.writeChar(PacketPlayer.Description);
	packet.writeInt16(pid);
	packet.writeString(value);
	packet.sendToAll(RELIABLE_ORDERED);
	packet = null;
}

function playAni(pid, value)
{
	local packet = Packet()
	packet.writeChar(PacketId.Player);
	packet.writeChar(PacketPlayer.Animation);
	packet.writeInt16(pid);
	packet.writeString(value);
	packet.sendToAll(RELIABLE_ORDERED);
	packet = null;	
}

function calculateCritChance(attr, weapondmg, chance)
{
	local _rand = rand() % 100;
	if(_rand <= chance)
		return attr + weapondmg;
	
	return 0;
}


class _Player
{
    id = -1
    pid = -1;

    items = null
    loggIn = false
    botProtection = false

    description = null

    password = null
    name = -1
    classId = -1
    fractionId = -1

    posx = 0;
    posy = 0;
    posz = 0;

    angle = 0;

    walk = 0

    str = 0
    dex = 0

    hpMax = 40
    manaMax = 10

    magicLvl = 0;

    body = "";
    skin = 0;
    head = "";
    face = 0;

    weapon = null

    constructor(playerId)
    {
        id = -1;
        pid = playerId;

        description = "";

        items = {};
        loggIn = false;
        botProtection = false;

        password = "";
        name = "";
        classId = 0;
        fractionId = 0;

        posx = 0;
        posy = 0;
        posz = 0;

        angle = 0;

        walk = 0;

        str = 10
        dex = 10

        hpMax = 40
        manaMax = 10

        magicLvl = 0;

        body = "";
        skin = 0;
        head = "";
        face = 0;

        weapon = [0,0,0,0]
    }

    function onSecond()
    {
 		if(loggIn == false)
			return;

		local position = getPlayerPosition(pid);
		posx = position.x; posy = position.y; posz = position.z;
		angle = getPlayerAngle(pid);
		callEvent("onPlayerPositionChange", pid, posx, posy, posz);
    }

    function respawn()
    {
        setPlayerPosition(pid, CFG.DefaultPosition.x, CFG.DefaultPosition.y, CFG.DefaultPosition.z);
        setPlayerAngle(pid, CFG.DefaultPosition.angle);
        spawnPlayer(pid);

        setPlayerStrength(pid, str);
        setPlayerDexterity(pid, dex);

        setPlayerMana(pid, manaMax);
        setPlayerMaxMana(pid, manaMax);

        setPlayerHealth(pid, hpMax);
        setPlayerMaxHealth(pid, hpMax);

        setPlayerVisual(pid, body, skin, head, face);

        setPlayerSkillWeapon(pid, 0, getPlayerSkillWeapon(pid, 0));
        setPlayerSkillWeapon(pid, 1, getPlayerSkillWeapon(pid, 1));
        setPlayerSkillWeapon(pid, 2, getPlayerSkillWeapon(pid, 2));
        setPlayerSkillWeapon(pid, 3, getPlayerSkillWeapon(pid, 3));

        loggIn = true;
    }

    function newAccount(username, pass)
    {
        local checkExist = Database.checkExistPlayer(username);
        if(checkExist)
        {
            addNotification(pid, "Podane konto ju� istnieje.");
            return;
        }

        name = username;
        password = pass;

        setPlayerName(pid, username);
        spawnPlayer(pid);
        setClassPlayer(pid, 0, 0);
        setPlayerVisual(pid, CFG.DefaultVisual.Body, CFG.DefaultVisual.Skin, CFG.DefaultVisual.Head, CFG.DefaultVisual.Face)

        id = Database.getNextId();
        Database.createPlayer(this);

        setPlayerPosition(pid, CFG.DefaultPosition.x, CFG.DefaultPosition.y, CFG.DefaultPosition.z);
        setPlayerAngle(pid, CFG.DefaultPosition.angle);

        loggIn = true;
        callEvent("onPlayerLoggIn", pid);

        local packet = Packet();
        packet.writeChar(PacketId.Player);
        packet.writeChar(PacketPlayer.Register);
        packet.send(pid, RELIABLE_ORDERED);
        packet = null;
    }

    function loadAccount(username, pass)
    {
        local checkExist = Database.checkExistPlayer(username);
        if(!checkExist)
        {
            addNotification(pid, "Podane konto nie istnieje.");
            return;
        }

        if(checkExist != pass)
        {
            addNotification(pid, "Z�e has�o.");
            return;
        }

        name = username;
        password = pass;

        setPlayerName(pid, username);
        spawnPlayer(pid);

        Database.loadAccount(this);
        Database.loadItems(pid);

        loggIn = true;
        callEvent("onPlayerLoggIn", pid);

        local packet = Packet();
        packet.writeChar(PacketId.Player);
        packet.writeChar(PacketPlayer.LoggIn);
        packet.send(pid, RELIABLE_ORDERED);
        packet = null;
    }

    function disconnect(reason)
    {
        if(loggIn)
        {
            Database.updatePlayer(this);
            Database.saveItems(pid);
        }

        id = -1;

        items = {};
        loggIn = false;
        botProtection = false;

        description = ""

        password = ""
        name = ""
        classId = 0;
        fractionId = 0;

        walk = 0;

        str = 10
        dex = 10

        hpMax = 40
        manaMax = 10

        magicLvl = 0;

        body = "";
        skin = 0;
        head = "";
        face = 0;

        weapon = [0,0,0,0]
    }
}

Player <- [];

for(local i = 0; i < getMaxSlots(); i ++)
    Player.append(_Player(i));


addEventHandler("onPacket", function(pid, packet) {
    local packetType = packet.readChar();
    if(packetType != PacketId.Player)
        return;

    packetType = packet.readChar();
    switch(packetType)
    {
        case PacketPlayer.Register:
            Player[pid].newAccount(packet.readString(), packet.readString())
        break;
        case PacketPlayer.LoggIn:
            Player[pid].loadAccount(packet.readString(), packet.readString())
        break;
        case PacketPlayer.Visual:
            setPlayerVisual(pid, packet.readString(), packet.readInt16(), packet.readString(), packet.readInt16());
        break;
        case PacketPlayer.Walk:
            setPlayerWalkingStyle(pid, packet.readInt16());
        break;
        case PacketPlayer.Trade:
            startPlayerTrade(pid, packet.readInt16(), packet.readString(), packet.readString(), packet.readInt16(), packet.readInt16())
        break;
        case PacketPlayer.UseItem:
            playerUseItemClient(pid, packet.readString(), packet.readInt16())
        break;
    }
})

addEventHandler("onPlayerRespawn", function(pid) {
    Player[pid].respawn();
})

addEventHandler("onPlayerDisconnect", function(pid, res) {
    Player[pid].disconnect(res);
})

addEventHandler("onSecond", function() {
    foreach(_player in Player)
        _player.onSecond();
})

addEventHandler("onPlayerTakeItem", function(pid, item) {
    local id = Items.id(item.instance);
    if(Player[pid].items.rawin(id))
        Player[pid].items[id] = Player[pid].items[id] + item.amount;
    else
        Player[pid].items[id] <- item.amount;
})

addEventHandler("onPlayerDropItem", function(pid, item) {
    local id = Items.id(item.instance);
    if(Player[pid].items.rawin(id))
    {
        if(Player[pid].items[id] > item.amount)
            Player[pid].items[id] = Player[pid].items[id] - item.amount;
        else
            Player[pid].items.rawdelete(id);
    }
})

addEventHandler("onPlayerJoin", function(pid) {
    local packet = Packet()
	packet.writeChar(PacketId.Player);
	packet.writeChar(PacketPlayer.Description);
	packet.writeInt16(pid);
	packet.writeString("");
	packet.sendToAll(RELIABLE_ORDERED);
	packet = null;

    for(local i = 0; i < getMaxSlots(); i ++)
    {
        if(!Player[i].loggIn)
            continue;

        local packet = Packet()
        packet.writeChar(PacketId.Player);
        packet.writeChar(PacketPlayer.Description);
        packet.writeInt16(i);
        packet.writeString(Player[i].description);
        packet.send(pid, RELIABLE_ORDERED);
        packet = null;
    }
})

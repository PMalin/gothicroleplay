
Database <- {};

function Database::getNextId()
{
	local pfile = io.file("database/id.cfg", "r");
    if(pfile.isOpen) {
        local id = pfile.read(io_type.LINE).tointeger(); id = id + 1;
        pfile.close();
        pfile = io.file("database/id.cfg", "w");
        pfile.write(id+"\n");
        pfile.close();
        return id;
    }else{
        pfile = io.file("database/id.cfg", "w");
        pfile.write("1");
        pfile.close();
        return 1;        
    }
}

function Database::checkExistPlayer(name)
{
	local pfile = io.file("database/"+ name + ".txt", "r");
	if (pfile.isOpen){
        local id = pfile.read(io_type.LINE);
        return pfile.read(io_type.LINE);
    }
    return false;
}

function Database::createPlayer(player)
{
    local pos = getPlayerPosition(player.pid);
    local visual = getPlayerVisual(player.pid);
	local pfile = io.file("database/"+ getPlayerName(player.pid) + ".txt", "w+");
    pfile.write(player.id + "\n");
    pfile.write(player.password + "\n");
    pfile.write(player.classId + "\n");
    pfile.write(player.fractionId + "\n");
    pfile.write(player.walk + "\n");
    pfile.write(pos.x + "\n");
    pfile.write(pos.y + "\n");
    pfile.write(pos.z + "\n");
    pfile.write(getPlayerAngle(player.pid) + " \n")
    pfile.write(player.str + "\n");
    pfile.write(player.dex + "\n");
    pfile.write(getPlayerHealth(player.pid) + "\n");
    pfile.write(player.hpMax + "\n");
    pfile.write(getPlayerMana(player.pid) + "\n");
    pfile.write(player.manaMax + "\n");
    pfile.write(visual.bodyModel + "\n");
    pfile.write(visual.bodyTxt + "\n");
    pfile.write(visual.headModel + "\n");
    pfile.write(visual.headTxt + "\n");
    pfile.write(player.weapon[0] + "\n");
    pfile.write(player.weapon[1] + "\n");
    pfile.write(player.weapon[2] + "\n");
    pfile.write(player.weapon[3] + "\n");
    pfile.write(player.magicLvl + "\n");
    pfile.write(player.description + "\n");
    pfile.close();
}

function Database::updatePlayer(player)
{
	local pfile = io.file("database/"+ getPlayerName(player.pid) + ".txt", "w");
	if (pfile.isOpen)
	{
        local pos = getPlayerPosition(player.pid);
        local visual = getPlayerVisual(player.pid);
		pfile.write(player.id + "\n");
		pfile.write(player.password + "\n");
		pfile.write(player.classId + "\n");
		pfile.write(player.fractionId + "\n");
		pfile.write(player.walk + "\n");
        pfile.write(pos.x + "\n");
        pfile.write(pos.y + "\n");
        pfile.write(pos.z + "\n");
        pfile.write(getPlayerAngle(player.pid) + " \n")
		pfile.write(player.str + "\n");
		pfile.write(player.dex + "\n");
		pfile.write(getPlayerHealth(player.pid) + "\n");
		pfile.write(player.hpMax + "\n");
		pfile.write(getPlayerMana(player.pid) + "\n");
		pfile.write(player.manaMax + "\n");
        pfile.write(visual.bodyModel + "\n");
		pfile.write(visual.bodyTxt + "\n");
		pfile.write(visual.headModel + "\n");
		pfile.write(visual.headTxt + "\n");
		pfile.write(player.weapon[0] + "\n");
		pfile.write(player.weapon[1] + "\n");
		pfile.write(player.weapon[2] + "\n");
		pfile.write(player.weapon[3] + "\n");
		pfile.write(player.magicLvl + "\n");
		pfile.write(player.description + "\n");
		pfile.close();
        return true;
	}
	else
		return false;
}

function Database::removeAccount(player)
{
	local pfile = io.file("database/"+ getPlayerName(player.pid) + ".txt", "w");
	if (pfile.isOpen)
	{
        pfile.close();
        return true;
    }
    return false;
}


function Database::loadAccount(player)
{
    local pid = player.pid;
	local pfile = io.file("database/"+ getPlayerName(pid) + ".txt", "r");
	
    player.id = pfile.read(io_type.LINE).tointeger();
    player.password = pfile.read(io_type.LINE);
    player.classId = pfile.read(io_type.LINE).tointeger();
    player.fractionId = pfile.read(io_type.LINE).tointeger();
    setPlayerWalkingStyle(pid, pfile.read(io_type.LINE).tointeger());
    
    local posX = pfile.read(io_type.LINE).tointeger();
    local posY = pfile.read(io_type.LINE).tointeger();
    local posZ = pfile.read(io_type.LINE).tointeger();
    local angle = pfile.read(io_type.LINE).tointeger();
    setPlayerPosition(pid, posX, posY, posZ);
    setPlayerAngle(pid, angle);

    setPlayerStrength(pid, pfile.read(io_type.LINE).tointeger());
    setPlayerDexterity(pid, pfile.read(io_type.LINE).tointeger());
    setPlayerHealth(pid, pfile.read(io_type.LINE).tointeger());
    setPlayerMaxHealth(pid, pfile.read(io_type.LINE).tointeger());
    setPlayerMana(pid, pfile.read(io_type.LINE).tointeger());
    setPlayerMaxMana(pid, pfile.read(io_type.LINE).tointeger());
    
    local body = pfile.read(io_type.LINE);
    local skin = pfile.read(io_type.LINE).tointeger();
    local head = pfile.read(io_type.LINE);
    local face = pfile.read(io_type.LINE).tointeger();

    setPlayerVisual(pid, body, skin, head, face);
    setPlayerSkillWeapon(pid, 0, pfile.read(io_type.LINE).tointeger())
    setPlayerSkillWeapon(pid, 1, pfile.read(io_type.LINE).tointeger())
    setPlayerSkillWeapon(pid, 2, pfile.read(io_type.LINE).tointeger())
    setPlayerSkillWeapon(pid, 3, pfile.read(io_type.LINE).tointeger())
    setPlayerMagicLevel(pid, pfile.read(io_type.LINE).tointeger())
    setPlayerDescription(pid, pfile.read(io_type.LINE));

    local packet = Packet()
    packet.writeChar(PacketId.Player);
    packet.writeChar(PacketPlayer.SetClass);
    packet.writeInt16(player.fractionId);
    packet.writeInt16(player.classId);
    packet.send(pid, RELIABLE_ORDERED);
    packet = null;       
}

function Database::saveItems(pid)
{
	local fileSave = io.file("database/items/"+getPlayerName(pid)+".txt", "w");
	if(fileSave){
		foreach(v,k in getPlayerItems(pid))
			fileSave.write(Items.name(v) + " " + k + "\n");
		
	fileSave.close();
	}else
        print(fileSave.errorMsg)	
}

function Database::loadItems(pid)
{
    local fileLoad = io.file("database/items/"+getPlayerName(pid)+".txt", "r");
    if (fileLoad.isOpen){
	local args = 0;
	    do{
            args = fileLoad.read(io_type.LINE);		
		    if(args != null){
				local arg = sscanf("sd", args);
				giveItem(pid, arg[0], arg[1]);
		    }		    
            else{
                break;
            }
        } while (args != null)     
	}
	fileLoad.close();
}
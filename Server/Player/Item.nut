_giveItem <- giveItem;
_removeItem <- removeItem;

function playerUseItemClient(pid, instance, amount)
{
    instance = instance.toupper();
	local id = Items.id(instance);
	if(id == -1)
		return;

    if(Player[pid].items.rawin(id))
    {
        if(Player[pid].items[id] > amount)
            Player[pid].items[id] = Player[pid].items[id] - amount;
        else
            Player[pid].items.rawdelete(id);
    }
}

function removeItem(pid, instance, amount)
{
	instance = instance.toupper();
	local id = Items.id(instance);
	if(id == -1)
		return;

    if(Player[pid].items.rawin(id))
    {
        if(Player[pid].items[id] > amount)
            Player[pid].items[id] = Player[pid].items[id] - amount;
        else
            Player[pid].items.rawdelete(id);
    }
    
    _removeItem(pid, id, amount);
}

function giveItem(pid, instance, amount)
{
	instance = instance.toupper();
	local id = Items.id(instance);
	if(id == -1)
		return;

    if(Player[pid].items.rawin(id))
        Player[pid].items[id] = Player[pid].items[id] + amount;
    else
        Player[pid].items[id] <- amount;

    _giveItem(pid, id, amount);
}

function getPlayerItems(pid)
{
	return Player[pid].items;
}

function hasPlayerItem(pid,instance)
{
	instance = instance.toupper();
	local id = Items.id(instance);
	if(id == -1)
		return 0;

    if(Player[pid].items.rawin(id))
        return Player[pid].items[id];

    return 0;
}
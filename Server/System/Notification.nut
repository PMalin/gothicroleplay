
function addNotification(pid, text)
{
    local packet = Packet();
    packet.writeChar(PacketId.Other);
    packet.writeChar(PacketOther.Notification);
    packet.writeString(text);
    packet.send(pid, RELIABLE_ORDERED);
    packet = null;
}
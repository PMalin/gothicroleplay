

Player <- {
    loggIn = false

    fractionId = -1,
    classId = -1,
    gui = -1,

    description = array(getMaxSlots(), "")
}

function getPlayerClass(pid)
{
    if(!Fraction.rawin(Player.fractionId))
        return "B��d";

    if(!Fraction[Player.fractionId].classes.rawin(Player.classId))
        return "B��d";

    return Fraction[Player.fractionId].classes[Player.classId].name;
}

Player.packetLoggIn <- function(name, password){
    local packet = Packet();
    packet.writeChar(PacketId.Player);
    packet.writeChar(PacketPlayer.LoggIn);
    packet.writeString(name);
    packet.writeString(password);
    packet.send(RELIABLE_ORDERED);
    packet = null;
}

Player.packetTrade <- function(focusId, instance, name, amount, gold){
    local packet = Packet();
    packet.writeChar(PacketId.Player);
    packet.writeChar(PacketPlayer.Trade);
    packet.writeInt16(focusId);
    packet.writeString(instance);
    packet.writeString(name);
    packet.writeInt16(amount);
    packet.writeInt16(gold);
    packet.send(RELIABLE_ORDERED);
    packet = null;
}

Player.packetRegister <- function(name, password){
    local packet = Packet();
    packet.writeChar(PacketId.Player);
    packet.writeChar(PacketPlayer.Register);
    packet.writeString(name);
    packet.writeString(password);
    packet.send(RELIABLE_ORDERED);
    packet = null;
}

Player.packetVisual <- function(bodyModel, bodyTxt, headModel, headTxt) {
    local packet = Packet();
    packet.writeChar(PacketId.Player);
    packet.writeChar(PacketPlayer.Visual);
    packet.writeString(bodyModel);
    packet.writeInt16(bodyTxt);
    packet.writeString(headModel);
    packet.writeInt16(headTxt);
    packet.send(RELIABLE_ORDERED);
    packet = null;
}

Player.packetWalk <- function(id) {
    local packet = Packet();
    packet.writeChar(PacketId.Player);
    packet.writeChar(PacketPlayer.Walk);
    packet.writeInt16(id);
    packet.send(RELIABLE_ORDERED);
    packet = null;
}

Player.onPacket <- function(packet)
{
    local packetType = packet.readChar();
    if(packetType != PacketId.Player)
        return;

    packetType = packet.readChar();
    switch(packetType)
    {
        case PacketPlayer.Register:
            Interface.Register.hide();
            Interface.baseInterface(false);
        break;
        case PacketPlayer.LoggIn:
            Interface.LoggIn.hide();
            Interface.baseInterface(false);
        break;
        case PacketPlayer.SetClass:
            Player.fractionId = packet.readInt16();
            Player.classId = packet.readInt16();
        break;
        case PacketPlayer.Description:
            Player.description[packet.readInt16()] = packet.readString();
        break;
        case PacketPlayer.Animation:
            playAni(packet.readInt16(), packet.readString());
        break;
    }
}

addEventHandler("onPacket", Player.onPacket);

function setPlayerWalkingStyle(pid, id)
{
    Player.packetWalk(id);
}


addEventHandler("onUseItem", function(instance, amount, hand)
{
	if(instance.find("ITPO_") != null || instance.find("ITSC_") != null || instance.find("ITFO_") != null || instance.find("ITPL_") != null){
        local packet = Packet();
        packet.writeChar(PacketId.Player);
        packet.writeChar(PacketPlayer.UseItem);
        packet.writeString(instance);
        packet.writeInt16(amount);
        packet.send(RELIABLE_ORDERED);
        packet = null;
	}
});


addEventHandler("onShoot", function() 
{
	local wp = getPlayerWeaponMode(heroId);
	if(wp == 5){
        local packet = Packet();
        packet.writeChar(PacketId.Player);
        packet.writeChar(PacketPlayer.UseItem);
        packet.writeString("ITRW_ARROW");
        packet.writeInt16(1);
        packet.send(RELIABLE_ORDERED);
        packet = null;
	}else if(wp == 6) {
        local packet = Packet();
        packet.writeChar(PacketId.Player);
        packet.writeChar(PacketPlayer.UseItem);
        packet.writeString("ITRW_BOLT");
        packet.writeInt16(1);
        packet.send(RELIABLE_ORDERED);
        packet = null;
    }
});
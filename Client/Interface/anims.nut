choiceAnimation <- false;

gridAnim <- GUI.GridList(0,2440,1800,4800, "MENU_INGAME.TGA", "INV_SLOT.TGA", "INV_TITEL.TGA", "O.TGA", "U.TGA")
gridAnim.setMarginPx(35,35,35,35)

animNameColumn <- gridAnim.addColumn("***", anx(280), Align.Center)
animNameColumn.draw.setColor(145, 175, 205)

function animationPanel(toggle){
    if(toggle && Player.gui != -1)
        return;

	choiceAnimation = toggle;

	if(toggle) 
		foreach(anim in CFG.Anims) gridAnim.addRow(_L(anim.name));
	else
		for (local i = 0;i <= gridAnim.rows.len(); i ++) gridAnim.removeRow(i);

	if(!toggle)
		try { for (local i = 0;i <= gridAnim.rows.len(); i ++) gridAnim.removeRow(i); } catch (error) {};

    Interface.baseInterface(toggle,toggle ? PLAYER_GUI.ANIMATION : -1);
	gridAnim.setVisible(toggle);
}

addEventHandler("GUI.onClick", function(self){
	if(!choiceAnimation)
		return

	if (!(self instanceof GUI.GridListCell))
		return

	playAni(heroId, CFG.Anims[self.parent.id].inst);
});

addEventHandler("GUI.onMouseIn", function(self){
	if(!choiceAnimation)
		return

	if (!(self instanceof GUI.GridListCell))
		return

  	self.setColor(255, 0, 0);
  	self.setFile("Menu_Choice_Back.TGA");
});

addEventHandler("GUI.onMouseOut", function(self){
	if(!choiceAnimation)
		return

	if (!(self instanceof GUI.GridListCell))
		return

	self.setColor(255, 255, 255);
	self.setFile("");
});

addEventHandler("onKey", function(key){
	if(key == KEY_F10)
		animationPanel(!choiceAnimation)
});



Interface <- {};

function Interface::baseInterface(value, id = -1)
{
    Camera.setFreeze(value);

    enableHud(HUD_HEALTH_BAR,!value);
    enableHud(HUD_MANA_BAR,!value);

    for(local i = 0; i<200; i++)
      disableKey(i, value);
    
    Player.gui = id;

    if(id != -1)
      callEvent("onGUIOpen");
    else
      callEvent("onGUIClose");

    disableKey(1, true);
    setFreeze(value)
	  setCursorVisible(value)
}


addEventHandler("GUI.onMouseIn", function(self)
{
    if(Player.gui == -1)
        return;

    if(self instanceof GUI.Button || self instanceof GUI.Input)
      self.setAlpha(200);
})

addEventHandler("GUI.onMouseOut", function(self)
{
    if(Player.gui == -1)
        return;

    if(self instanceof GUI.Button || self instanceof GUI.Input)
      self.setAlpha(255);
})
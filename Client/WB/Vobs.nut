
vobsSelection <- {};

vobsSelection.active <- false;
vobsSelection.cat <- "Wszystkie";
vobsSelection.sId <- 0;
vobsSelection.vob <- 0;

vobsSelection.renders <- [];
vobsSelection.catButtons <- [];

vobsSelection.window <- GUI.Window(50, any(30), anx(600), any(600), "MENU_INGAME.TGA");
vobsSelection.slider <- GUI.Slider(anx(570), any(150), anx(30), any(400) anx(3), any(7), "MENU_INGAME.TGA", "BLACK.TGA", "RED.TGA", Orientation.Vertical, Align.Left, vobsSelection.window)

vobsSelection.clickers <- [
    GUI.Button(anx(20), any(150), anx(150), any(130), "MENU_INGAME.TGA", "", vobsSelection.window),
    GUI.Button(anx(200), any(150), anx(150), any(130), "MENU_INGAME.TGA", "", vobsSelection.window),
    GUI.Button(anx(380), any(150), anx(150), any(130), "MENU_INGAME.TGA", "", vobsSelection.window),
    GUI.Button(anx(20), any(290), anx(150), any(130), "MENU_INGAME.TGA", "", vobsSelection.window),
    GUI.Button(anx(200), any(290), anx(150), any(130), "MENU_INGAME.TGA", "", vobsSelection.window),
    GUI.Button(anx(380), any(290), anx(150), any(130), "MENU_INGAME.TGA", "", vobsSelection.window),
    GUI.Button(anx(20), any(430), anx(150), any(130), "MENU_INGAME.TGA", "", vobsSelection.window),
    GUI.Button(anx(200), any(430), anx(150), any(130), "MENU_INGAME.TGA", "", vobsSelection.window),
    GUI.Button(anx(380), any(430), anx(150), any(130), "MENU_INGAME.TGA", "", vobsSelection.window),
];

foreach(val in vobsSelection.clickers)
    val.setColor(255,0,0);

vobsSelection.window.setColor(255, 0, 0);

function vobsSelection::start()
{
    active = true;
    window.setVisible(true);

    local id = 0;
    foreach(_cat, _val in builder.list)
    {
        local y = id/4;y = y.tointeger();
        local button = GUI.Button(anx(20 + (id*135)), any(20 + (y*60)), anx(130), any(60), "RED.TGA", _cat, vobsSelection.window);
        vobsSelection.catButtons.append(button);
        id++;
    }

    foreach(_butt in vobsSelection.catButtons)
    {
        _butt.setVisible(true);
    }

    openRenders();
}

function vobsSelection::end()
{
    active = false;
    window.setVisible(false);
    vobsSelection.renders.clear();
}

function vobsSelection::openRenders()
{
    local listWhole = builder.list[vobsSelection.cat];
    vobsSelection.renders.clear();

    if(listWhole.len() <= 9)
        vobsSelection.slider.setVisible(false);
    else {
        vobsSelection.slider.setMaximum((listWhole.len()/3).tointeger() + 1);
        vobsSelection.slider.setVisible(true);
    }

    foreach(id, clicker in vobsSelection.clickers)
    {
        local rId = id + vobsSelection.vob;
        if((listWhole.len()-1) >= rId)
        {
            local pos = clicker.getPosition();
            local item = ItemRender(pos.x, pos.y, anx(150), any(130), listWhole[rId].name);

            clicker.setText(rId+"/"+listWhole.len())
            item.visible = true;
            item.lightingswell = true;
            clicker.draw.top();
            vobsSelection.renders.append(item);
        }
    }
}

addEventHandler("GUI.onClick", function(self)
{
    if(!isBuilderActive())
        return;

    foreach(_butt in vobsSelection.catButtons)
    {
        if(_butt == self)
        {
            vobsSelection.cat = _butt.getText();
            vobsSelection.openRenders();
            return;
        }
    }
    foreach(id, clicker in vobsSelection.clickers)
    {
        if(clicker != self)
            continue;

        vobsSelection.sId = vobsSelection.vob + id;
        setBuilderVobCollisionOn();
        builder.changeVob(builder.list[vobsSelection.cat][vobsSelection.sId].name);
        break;
    }
})

addEventHandler("GUI.onChange", function(self)
{
    if(!isBuilderActive())
        return;
        
	if(vobsSelection.slider != self)
        return;
    
    local val = self.getValue();
    vobsSelection.vob = val * 3;
    vobsSelection.openRenders()
})
